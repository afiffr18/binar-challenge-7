package id.afif.binarchallenge7

import id.afif.binarchallenge7.helper.toDate
import id.afif.binarchallenge7.helper.toHourMinutes
import id.afif.binarchallenge7.helper.toPercentage
import id.afif.binarchallenge7.helper.toYear
import org.junit.Assert.assertEquals
import org.junit.Test

class ExtFunctionTest {

    @Test
    fun formatDate() {
        val date = "2022-01-01"
        assertEquals("Januari 01, (2022)", date.toDate())
    }

    @Test
    fun formatHour() {
        val waktu = 120
        assertEquals("2 Jam 0 Menit", waktu.toHourMinutes())
    }

    @Test
    fun floatToPercentage() {
        val vote = 6.7
        assertEquals("67%", vote.toPercentage())
    }

    @Test
    fun getYear() {
        val date = "2022-01-02"
        assertEquals("(2022)", date.toYear())
    }

}