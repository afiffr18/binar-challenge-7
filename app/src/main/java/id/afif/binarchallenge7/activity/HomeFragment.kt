package id.afif.binarchallenge7.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import id.afif.binarchallenge7.Adapter.MoviesAdapter
import id.afif.binarchallenge7.Model.Status
import id.afif.binarchallenge7.R
import id.afif.binarchallenge7.databinding.FragmentHomeBinding
import id.afif.binarchallenge7.viewmodel.MoviesViewModel
import id.afif.binarchallenge7.viewmodel.UserViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var moviesAdapter: MoviesAdapter

    private val viewModel: MoviesViewModel by viewModel()
    private val userViewModel: UserViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        getDataFromNetwork()
        setUsernameLogin()
        profileClicked()
        favoritesClicked()
    }

    private fun initRecycler() {
        moviesAdapter = MoviesAdapter { id: Int ->
            val bundle = Bundle()
            bundle.putInt("id", id)
            findNavController().navigate(R.id.action_homeFragment_to_detailFragment, bundle)
        }
        binding.apply {
            rvMovies.adapter = moviesAdapter
            rvMovies.layoutManager = GridLayoutManager(requireContext(), 2)
        }
    }

    private fun getDataFromNetwork() {
        viewModel.getAllMovies().observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.pbLoading.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.pbLoading.isVisible = false
                    it.data?.let { data ->
                        moviesAdapter.updateData(data.results)
                    }
                }
                Status.ERROR -> {
                    binding.pbLoading.isVisible = false
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

            }
        }
    }


    private fun setUsernameLogin() {
        val username = arguments?.getString("username")
        val password = arguments?.getString("password")

        if (username != null || password != null) {
            userViewModel.saveData(username!!, password!!)
            viewModel.getDataById(username, password)
        } else {
            userViewModel.getUserLogin().observe(viewLifecycleOwner) {
                viewModel.getDataById(it[0], it[1])
            }
        }


        viewModel.dataUser.observe(viewLifecycleOwner) {
            val welcomeUser = "welcome : ${it.username}"
            binding.tvUserLogin.text = welcomeUser
            it.id?.let { it1 ->
                userViewModel.saveId(it1)
            }
        }


    }

    private fun profileClicked() {
        binding.btnProfile.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
    }

    private fun favoritesClicked() {
        binding.btnFavorites.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_favoritesFragment)
        }
    }

}