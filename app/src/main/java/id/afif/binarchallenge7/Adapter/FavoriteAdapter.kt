package id.afif.binarchallenge7.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.progressindicator.CircularProgressIndicator
import id.afif.binarchallenge7.R
import id.afif.binarchallenge7.database.Favorite
import id.afif.binarchallenge7.helper.toPercentage


class FavoriteAdapter(private val onClickListener: (id: Int) -> Unit) :
    RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Favorite>() {
        override fun areItemsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun updateData(favorite: List<Favorite>) = differ.submitList(favorite)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.card_favorites, parent, false)
        return FavoriteViewHolder(view)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val ivPoster = view.findViewById<ImageView>(R.id.iv_movies)
        private val tvTitle = view.findViewById<TextView>(R.id.tv_movie_title)
        private val tvDate = view.findViewById<TextView>(R.id.tv_movie_date)
        private val rating = view.findViewById<CircularProgressIndicator>(R.id.progress_circular)
        private val persen = view.findViewById<TextView>(R.id.tv_progress_percentage)
        private val constLayout = view.findViewById<ConstraintLayout>(R.id.constraint1)

        private val bgOptions = RequestOptions().placeholder(R.drawable.ic_launcher_background)
        fun bind(result: Favorite) {
            tvTitle.text = result.title
            tvDate.text = result.releaseDate
            Glide.with(itemView.context)
                .load("https://www.themoviedb.org/t/p/w220_and_h330_face/${result.posterPath}")
                .apply(bgOptions).into(ivPoster)
            rating.progress = (result.voteAverage * 10).toInt()
            persen.text = result.voteAverage.toPercentage()

            constLayout.setOnClickListener {
                onClickListener.invoke(result.id)
            }
        }

    }
}