package id.afif.binarchallenge7.Model

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}