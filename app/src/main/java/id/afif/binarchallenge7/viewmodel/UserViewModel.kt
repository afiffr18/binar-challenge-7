package id.afif.binarchallenge7.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import id.afif.binarchallenge7.helper.DataStoreManager
import kotlinx.coroutines.launch

class UserViewModel(private val dataStoreManager: DataStoreManager) : ViewModel() {

//    private val _dataUserLogin = MutableLiveData<Array<String>>()
//    val dataUserLogin: LiveData<Array<String>> get() = _dataUserLogin

    fun saveData(username: String, password: String) {
        viewModelScope.launch {
            dataStoreManager.setData(username, password)
        }
    }

    fun saveId(userId: Int) {
        viewModelScope.launch {
            dataStoreManager.setId(userId)
        }
    }

    fun getLoginId(): LiveData<Int> {
        return dataStoreManager.getIdLogin().asLiveData()
    }

    fun logoutSession() {
        viewModelScope.launch {
            dataStoreManager.logoutSession()
        }
    }

    fun getUserLogin(): LiveData<Array<String>> {
        return dataStoreManager.getUserLogin().asLiveData()
    }

//    fun getUserLogin() {
//        val userLogin = dataStoreManager.getUserLogin().asLiveData().value
//        _dataUserLogin.postValue(userLogin!!)
//    }

    fun getUsernameLogin(): LiveData<String> {
        return dataStoreManager.getUsernameLogin().asLiveData()
    }

    fun getPasswordLogin(): LiveData<String> {
        return dataStoreManager.getPasswordLogin().asLiveData()
    }

    fun isLoggin(): LiveData<Boolean> {
        return dataStoreManager.isLoggin().asLiveData()
    }

}