package id.afif.binarchallenge7

import android.app.Application
import id.afif.binarchallenge7.helper.DataStoreManager
import id.afif.binarchallenge7.helper.UserRepo
import id.afif.binarchallenge7.viewmodel.MoviesViewModel
import id.afif.binarchallenge7.viewmodel.UserViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.GlobalContext.startKoin
import org.koin.dsl.module

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule))
        }
    }

    private val appModule = module {
        single { UserRepo(androidContext()) }
        viewModel { MoviesViewModel(get()) }
        single { DataStoreManager(androidContext()) }
        viewModel { UserViewModel(get()) }
    }
}